from model import db, Questions, Options


def find_question_from_id(question_id):
    question = Questions.query.filter_by(id=question_id).first()
    return question


def all_questions():
	all_questions_list = Questions.query.all()
	return all_questions_list


def find_correct_answer_from_question(question_id):
	option = Options.query.filter_by(question_id=question_id, is_correct=True).first()
	return option.text