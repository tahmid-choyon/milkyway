import random

CORRECT_ANSWER_RESPONSES = [
    'আপনি চমৎকার খেলছেন! আপনার উত্তরটি সঠিক হয়েছে!',
    'বাহ্! আপনি মঙ্গল গ্রহের ব্যাপারে অনেক কিছুই জানেন! সঠিক উত্তর!',
    'সঠিক উত্তর! আপনি অসাধারণ খেলছেন!'
]

WRONG_ANSWER_RESPONSES = [
    'আপনার উত্তরটি ভুল হয়েছে! আপনি আরেকবার আমাদের মঙ্গল গ্রহের প্রামাণ্য চিত্র থেকে ঘুরে আসুন!',
    'ভুল উত্তর! আপনি ভালো খেলছেন এখনো! পরের গুলো আশা করি সঠিক হবে!'
]

TEMP_WA = []
TEMP_CA = []


def get_response():
    global CORRECT_ANSWER_RESPONSES, WRONG_ANSWER_RESPONSES
    global TEMP_CA, TEMP_WA

    wrong_ans_response = 'Wrong answer'
    correct_ans_response = 'Correct!'

    if len(TEMP_CA) == 0:
        TEMP_CA = list(CORRECT_ANSWER_RESPONSES)
    random.shuffle(TEMP_CA)
    correct_ans_response = TEMP_CA.pop()

    if len(TEMP_WA) == 0:
        TEMP_WA = list(WRONG_ANSWER_RESPONSES)
    random.shuffle(TEMP_WA)
    wrong_ans_response = TEMP_WA.pop()

    return correct_ans_response, wrong_ans_response
