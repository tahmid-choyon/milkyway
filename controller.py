from flask import jsonify, request

import crud
import utilities
from model import app, db, Options, Questions


def add_cors_header(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'HEAD, GET, POST, PATCH, PUT, OPTIONS, DELETE'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type, *'
    response.headers['Access-Control-Allow-Credentials'] = 'true'

    return response


app.after_request(add_cors_header)


@app.route('/')
def home():
    return jsonify(
        {
            "status": "success",
            "message": "Galaxy QA API"
        }
    ), 200


@app.route('/api/v1/question', methods=['GET', 'POST'])
def add_question():
    if request.method == 'GET':
        all_questions = crud.all_questions()
        all_questions = [question.json_dump() for question in all_questions]
        return jsonify(
            {
                "status": "success",
                "total_question": len(all_questions),
                "questions": all_questions
            }
        ), 200
    elif request.method == 'POST':
        question_data = request.get_json()
        question_body = question_data['question_body'].encode('utf-8')
        total_options = len(question_data['options'])
        new_question = Questions(body=question_body, total_options=total_options)
        db.session.add(new_question)
        db.session.commit()

        for option in question_data['options']:
            text = option['text'].encode('utf-8')
            is_correct = option['is_correct']
            op = Options(question_id=new_question.id, text=text, is_correct=is_correct)
            db.session.add(op)
        db.session.commit()

        return jsonify(new_question.json_dump()), 200


@app.route('/api/v1/question/<int:question_id>', methods=['GET'])
def question(question_id=-1):
    if question_id != -1:
        question = crud.find_question_from_id(question_id=question_id)
        if question is not None:
            question_json = question.json_dump()
            question_json = utilities.randomize_options_and_mark_correct_ans(question=question_json)
            return jsonify(question_json), 200
        else:
            return jsonify(
                {
                    "status": "failed",
                    "message": "invalid question id"
                }
            ), 403
    else:
        return jsonify(
            {
                "status": "failed",
                "message": "invalid question id"
            }
        ), 403
